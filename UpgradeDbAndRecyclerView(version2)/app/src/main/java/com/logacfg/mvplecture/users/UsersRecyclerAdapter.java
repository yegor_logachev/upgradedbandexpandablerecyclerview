package com.logacfg.mvplecture.users;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.logacfg.mvplecture.R;
import com.logacfg.mvplecture.model.Phone;
import com.logacfg.mvplecture.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yegor on 8/5/17.
 */

public class UsersRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int USER_VIEW_TYPE = 0;
    private static final int PHONE_VIEW_TYPE = 1;

    private List<ItemInfo> itemInfoList;
    private LayoutInflater inflater;

    UsersRecyclerAdapter(Context context) {
        inflater = LayoutInflater.from(context);
    }

    public void setUsers(List<User> users) {
        itemInfoList = new ArrayList<>();
        prepareData(users);
        notifyDataSetChanged();
    }

    private void prepareData(List<User> users) {
        if (users != null) {
            for (User user : users) {
                UserItemInfo userItemInfo = new UserItemInfo(user);
                itemInfoList.add(userItemInfo);
                if (userItemInfo.isExpanded) {
                    addPhonesToInfoList(user);
                }
            }
        }
    }

    private void addPhonesToInfoList(User user) {
        List<Phone> phones = user.getPhones();
        if (phones != null) {
            for (Phone phone : phones) {
                itemInfoList.add(new PhoneItemInfo(phone));
            }
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layoutRes = viewType == USER_VIEW_TYPE ? R.layout.item_large : R.layout.item_list;
        View itemView = inflater.inflate(layoutRes, parent, false);
        return viewType == USER_VIEW_TYPE ? new UsersViewHolder(itemView) : new PhoneViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ItemInfo itemInfo = itemInfoList.get(position);
        if (holder instanceof UsersViewHolder) {
            UserItemInfo userItemInfo = (UserItemInfo) itemInfo;
            onBindUserViewHolder((UsersViewHolder) holder, userItemInfo.user);
        } else {
            PhoneItemInfo phoneItemInfo = (PhoneItemInfo) itemInfo;
            onBindPhoneViewHolder((PhoneViewHolder) holder, phoneItemInfo.phone);
        }
    }

    private void onBindUserViewHolder(UsersViewHolder holder, User user) {
        holder.nameTextView.setText(user.getName());
        holder.addressTextView.setText(user.getAddress());
    }

    private void onBindPhoneViewHolder(PhoneViewHolder holder, Phone phone) {
        holder.phoneTextView.setText(phone.getPhone());
    }

    @Override
    public int getItemViewType(int position) {
        int viewType;
        ItemInfo itemInfo = itemInfoList.get(position);
        if (itemInfo instanceof UserItemInfo) {
            viewType = USER_VIEW_TYPE;
        } else if (itemInfo instanceof PhoneItemInfo) {
            viewType = PHONE_VIEW_TYPE;
        } else {
            throw new IllegalArgumentException("Unknown view type for position: " + position);
        }
        return viewType;
    }

    @Override
    public int getItemCount() {
        return itemInfoList.size();
    }

    private class UsersViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView nameTextView;
        private TextView addressTextView;

        UsersViewHolder(final View itemView) {
            super(itemView);
            nameTextView = itemView.findViewById(R.id.textViewName);
            addressTextView = itemView.findViewById(R.id.textViewAddress);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int adapterPosition = getAdapterPosition();
            UserItemInfo itemInfo = (UserItemInfo) itemInfoList.get(adapterPosition);
            List<Phone> phones = itemInfo.user.getPhones();
            itemInfo.toggle();
            if (itemInfo.isExpanded) {
                expandItems(adapterPosition, phones);
            } else {
                collapseItems(adapterPosition, phones.size());
            }
        }

        private void collapseItems(int position, int size) {
            for (int i = 1; i <= size; i++) {
                itemInfoList.remove(position + 1);
            }
            notifyItemRangeRemoved(position + 1, size);
        }

        private void expandItems(int position, List<Phone> phones) {
            for (int i = 1; i <= phones.size(); i++) {
                PhoneItemInfo itemInfo = new PhoneItemInfo(phones.get(i - 1));
                itemInfoList.add(position + i, itemInfo);
            }
            notifyItemRangeInserted(position + 1, phones.size());
        }
    }

    private class PhoneViewHolder extends RecyclerView.ViewHolder {

        TextView phoneTextView;

        PhoneViewHolder(View itemView) {
            super(itemView);
            phoneTextView = (TextView) itemView;
        }
    }

    private interface ItemInfo {}

    private class UserItemInfo implements ItemInfo {

        private User user;
        private boolean isExpanded = false;

        UserItemInfo(User user) {
            this.user = user;
        }

        void toggle() {
            isExpanded = !isExpanded;
        }
    }

    private class PhoneItemInfo implements ItemInfo {

        private Phone phone;

        PhoneItemInfo(Phone phone) {
            this.phone = phone;
        }
    }
}
