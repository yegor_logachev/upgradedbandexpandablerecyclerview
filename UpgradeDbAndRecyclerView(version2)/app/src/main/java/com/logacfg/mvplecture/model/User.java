package com.logacfg.mvplecture.model;

import android.content.ContentValues;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yegor on 7/29/17.
 */

public class User {

    public static final String TABLE_NAME_V1 = "Users";
    public static final String TABLE_NAME_V2 = "Users_V2";
    public static final String ID = "_id";
    public static final String NAME = "name";
    public static final String SECOND_NAME = "second_name";
    public static final String PHONE_V1 = "phone";
    public static final String ADDRESS = "address";

    public static final String CREATE_SQL = "CREATE TABLE " + TABLE_NAME_V2 + " (" +
            User.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            User.NAME + " TEXT NOT NULL, " +
            User.SECOND_NAME + " TEXT, " +
            User.ADDRESS + " TEXT);";

    public static final String[] COLUMNS = {ID, NAME, SECOND_NAME, ADDRESS};

    private long id;
    private String name;
    private String secondName;
    private String address;
    private List<Phone> phones = new ArrayList<>();

    public User(String name, String secondName, String address) {
        this.name = name;
        this.secondName = secondName;
        this.address = address;
    }

    public User(Cursor cursor) {
        int index = cursor.getColumnIndex(ID);
        id = cursor.getLong(index);
        index = cursor.getColumnIndex(User.NAME);
        name = cursor.getString(index);
        index = cursor.getColumnIndex(User.SECOND_NAME);
        secondName = cursor.getString(index);
        index = cursor.getColumnIndex(User.ADDRESS);
        address = cursor.getString(index);
    }

    public ContentValues getContentValues() {
        ContentValues values = new ContentValues();
        values.put(User.NAME, name);
        values.put(User.SECOND_NAME, secondName);
        values.put(User.ADDRESS, address);
        return values;
    }

    public String getName() {
        return name + " " + secondName;
    }

    public String getAddress() {
        return address;
    }

    public void addPhone(Phone phone) {
        phones.add(phone);
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public long getId() {
        return id;
    }

    @Override
    public String toString() {
        return name + " " + secondName;
    }
}
