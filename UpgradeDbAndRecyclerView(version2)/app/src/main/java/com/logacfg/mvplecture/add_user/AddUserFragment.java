package com.logacfg.mvplecture.add_user;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.logacfg.mvplecture.R;
import com.logacfg.mvplecture.model.User;

/**
 * Created by Yegor on 8/5/17.
 */

public class AddUserFragment extends Fragment implements View.OnClickListener, AddUserContract.View, TextWatcher {

    private static final String USER_ID = "user_id";

    private EditText nameEditText;
    private EditText secondNameEditText;
    private EditText addressEditText;
    private EditText phoneEditText;
    private Button confirmButton;
    private AddUserContract.Presenter presenter;

    public static long getUserId(Intent intent) {
        return intent.getLongExtra(USER_ID, -1);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_add_user, container, false);
        nameEditText = root.findViewById(R.id.editTextName);
        nameEditText.addTextChangedListener(this);
        secondNameEditText = root.findViewById(R.id.editTextSecondName);
        addressEditText = root.findViewById(R.id.editTextAddress);
        phoneEditText = root.findViewById(R.id.editTextPhone);
        confirmButton = root.findViewById(R.id.buttonConfirm);
        confirmButton.setOnClickListener(this);
        return root;
    }

    @Override
    public void onClick(View view) {
        String name = nameEditText.getText().toString().trim();
        String secondName = secondNameEditText.getText().toString().trim();
        String address = addressEditText.getText().toString().trim();
//        String phone = phoneEditText.getText().toString().trim();
        User user = new User(name, secondName, address);
        presenter.addUser(user);
    }

    @Override
    public void setPresenter(AddUserContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

    @Override
    public void afterTextChanged(Editable editable) {
        confirmButton.setEnabled(!editable.toString().trim().isEmpty());
    }

    @Override
    public void onUserAdded(long id) {
        FragmentActivity activity = getActivity();
        Intent data = new Intent();
        data.putExtra(USER_ID, id);
        activity.setResult(Activity.RESULT_OK, data);
        activity.finish();
    }
}
