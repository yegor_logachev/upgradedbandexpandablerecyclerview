package com.logacfg.mvplecture.users;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.logacfg.mvplecture.R;
import com.logacfg.mvplecture.model.Phone;
import com.logacfg.mvplecture.model.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Yegor on 8/5/17.
 */

public class UsersRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int USER_VIEW_TYPE = 0;
    private static final int PHONE_VIEW_TYPE = 1;

    private List<User> users;
    private Map<Integer, List<Phone>> cache = new HashMap<>();
    private LayoutInflater inflater;

    public UsersRecyclerAdapter(Context context) {
        inflater = LayoutInflater.from(context);
    }

    public void setUsers(List<User> users) {
        this.users = users;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layoutRes = viewType == USER_VIEW_TYPE ? R.layout.item_large : R.layout.item_list;
        View itemView = inflater.inflate(layoutRes, parent, false);
        return viewType == USER_VIEW_TYPE ? new UsersViewHolder(itemView) : new PhoneViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int userIndex = getUserIndexByPosition(position);
        User user = users.get(userIndex);
        if (holder instanceof UsersViewHolder) {
            onBindUserViewHolder((UsersViewHolder) holder, user);
        } else {
            int phoneIndex = getPhoneIndexByPosition(position);
            List<Phone> phones = user.getPhones();
            onBindPhoneViewHolder((PhoneViewHolder) holder, phones.get(phoneIndex));
        }
    }


    private int getUserIndexByPosition(int position) {
        int counter = 0;
        while (position > 0) {
            position -= (getPhonesCount(counter) + 1);
            counter++;
        }
        return position < 0 ? counter - 1 : counter;
    }

    private int getPhoneIndexByPosition(int position) {
        int counter = 0;
        while (position > 0) {
            position -= (getPhonesCount(counter++) + 1);
            counter++;
        }
        int phonesCount = getPhonesCount(counter - 1);
        return phonesCount + position;
    }

    private void onBindUserViewHolder(UsersViewHolder holder, User user) {
        holder.nameTextView.setText(user.getName());
        holder.addressTextView.setText(user.getAddress());
    }

    private void onBindPhoneViewHolder(PhoneViewHolder holder, Phone phone) {
        holder.phoneTextView.setText(phone.getPhone());
    }

    @Override
    public int getItemViewType(int position) {
        if (position > 0) {
            int counter = 0;
            while (position > 0) {
                position -= (getPhonesCount(counter) + 1);
                counter++;
            }
        }
        return position == 0 ? USER_VIEW_TYPE : PHONE_VIEW_TYPE;
    }

    @Override
    public int getItemCount() {
        int items = 0;
        int usersCount = getUsersCount();
        for (int i = 0; i < usersCount; i++) {
            items += getPhonesCount(i);
        }
        items += usersCount;
        return items;

    }

    private int getUsersCount() {
        return users != null ? users.size() : 0;
    }

    private int getPhonesCount(int position) {
        return users.get(position).getPhones().size();
    }

    public void addUserAsFirst(User user) {
        users.add(0, user);
        notifyItemInserted(0);
    }

    private class UsersViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView nameTextView;
        private TextView addressTextView;

        UsersViewHolder(final View itemView) {
            super(itemView);
            nameTextView = itemView.findViewById(R.id.textViewName);
            addressTextView = itemView.findViewById(R.id.textViewAddress);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int adapterPosition = getAdapterPosition();
            int userIndex = getUserIndexByPosition(adapterPosition);
            if (cache.containsKey(userIndex)) {
                expandItems();
            } else {
                collapseItems();
            }
        }

        private void collapseItems() {
            int adapterPosition = getAdapterPosition();
            int userIndex = getUserIndexByPosition(adapterPosition);
            User user = users.get(userIndex);
            List<Phone> userPhones = user.getPhones();
            List<Phone> phones = new ArrayList<>(userPhones);
            cache.put(userIndex, phones);
            userPhones.clear();
            int size = phones.size();
            notifyItemRangeRemoved(adapterPosition + 1, size);
        }

        private void expandItems() {
            int adapterPosition = getAdapterPosition();
            int userIndex = getUserIndexByPosition(adapterPosition);
            User originalUser = users.get(userIndex);
            List<Phone> cachedPhones = cache.get(userIndex);
            cache.remove(userIndex);
            List<Phone> originalPhones = originalUser.getPhones();
            originalPhones.addAll(cachedPhones);
            notifyItemRangeInserted(adapterPosition + 1, originalPhones.size());
        }
    }

    private class PhoneViewHolder extends RecyclerView.ViewHolder {

        TextView phoneTextView;

        PhoneViewHolder(View itemView) {
            super(itemView);
            phoneTextView = (TextView) itemView;
        }
    }
}
