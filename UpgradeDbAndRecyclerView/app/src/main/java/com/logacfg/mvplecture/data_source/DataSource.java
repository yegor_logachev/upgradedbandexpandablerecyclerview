package com.logacfg.mvplecture.data_source;

import com.logacfg.mvplecture.model.User;

import java.util.List;

/**
 * Created by Yegor on 7/29/17.
 */

public interface DataSource {

    void insertUsers(List<User> users);

    long insertUser(User user);

    List<User> loadUsers();

    User getUserById(long id);

    boolean isPreInited();

    void setPreInited(boolean preInited);
}
