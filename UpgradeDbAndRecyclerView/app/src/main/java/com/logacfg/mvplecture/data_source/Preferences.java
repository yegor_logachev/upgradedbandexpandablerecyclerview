package com.logacfg.mvplecture.data_source;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Yegor on 8/12/17.
 */

public class Preferences {

    private static final String PREFERENCES_NAME = "Preferences";
    private static final String PREINITED_KEY = "preinited_key";

    private SharedPreferences preferences;

    public Preferences(Context context) {
        preferences = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    public boolean isPreInited() {
        return preferences.getBoolean(PREINITED_KEY, false);
    }

    public void setPreInited(boolean preInited) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(PREINITED_KEY, preInited);
        editor.apply();
    }
}
